package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    
  
   
   public Ponto(double x, double y, double z){
       this.x = x;
       this.y = y;
       this.z = z;
   }
   
    @Override
    public String toString(){
        return getNome() + "(" + getX() + "," + getY() + "," + getZ() + ")";
    }
    
   
    
    public double dist(Ponto ponto){
        double dx = ponto.x - getX();
        double dy = ponto.y = getY();
        double dz = ponto.z = getZ();
        return Math.sqrt(Math.pow(dx,2) + Math.pow(y,2) + Math.pow(z,2));
    }
   
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

}
