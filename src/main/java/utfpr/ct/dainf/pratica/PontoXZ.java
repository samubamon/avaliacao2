/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author samu
 */
public class PontoXZ extends Ponto2D{

    public PontoXZ(double x, double y, double z) {
        super(x, y, z);
    }
    
    
    
    @Override
        public String toString(){
        return getNome() + "(" + getX() + "," + getY() + "," + getZ() + ")";
    }
    
}
